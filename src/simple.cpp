#include "cpp11.hpp"


[[cpp11::register]]
cpp11::doubles square_cpp(cpp11::doubles input_vec) {
    cpp11::writable::doubles squared_vec(input_vec.size());

    for (int vec_ndx = 0; vec_ndx < input_vec.size(); vec_ndx++) {
        squared_vec[vec_ndx] = input_vec[vec_ndx] * input_vec[vec_ndx];
    }

    return squared_vec;
}
