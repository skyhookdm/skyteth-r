/**
 * Author: Aldrin Montana
 */

// ------------------------------
// Dependencies

#include "cpp11.hpp"

#include <arrow/api.h>
#include <arrow/io/api.h>
#include <arrow/ipc/api.h>
#include <arrow/filesystem/api.h>
#include <arrow/dataset/api.h>

#include <skytether/skytether.hpp>
#include <skytether/skytether_types.hpp>
#include <skytether/connectors/connectors.hpp>


// ------------------------------
// Macros


// ------------------------------
// Global variables


// ------------------------------
// Common API

[[cpp11::register]]
arrow::Result<BatchStreamRdrPtr>
R_GetReaderForKey(cpp11::r_string table_key) {
    arrow::Result<BatchStreamRdrPtr> reader_result;

    KineticConn kconn = KineticConn();
    kconn.Connect();

    if (kconn.IsConnected()) {
        reader_result = kconn.GetReaderForKey(static_cast<std::string>(table_key));
        kconn.Disconnect();
    }
    else {
        reader_result = arrow::Result<BatchStreamRdrPtr>(
            arrow::Status::Invalid("Could not connect to kinetic drive")
        );
    }

    return reader_result;
}

/*
#include <iostream>
arrow::Result<BatchStreamRdrPtr>
R_GetReaderForKey(std::string table_key) {
    arrow::Result<BatchStreamRdrPtr> reader_result;

    KineticConn kconn = KineticConn();
    kconn.Connect();

    if (kconn.IsConnected()) {
        reader_result = kconn.GetReaderForKey(static_cast<std::string>(table_key));
        kconn.Disconnect();
    }
    else {
        reader_result = arrow::Result<BatchStreamRdrPtr>(
            arrow::Status::Invalid("Could not connect to kinetic drive")
        );
    }

    return reader_result;
}
arrow::Status
R_PrintBatchExcerpt(arrow::Result<BatchStreamRdrPtr> reader_result) {
    RecordBatchPtr parsed_recordbatch;
    ARROW_ASSIGN_OR_RAISE(
         BatchStreamRdrPtr batch_reader
        ,reader_result
    );

    ARROW_RETURN_NOT_OK(batch_reader->ReadNext(&parsed_recordbatch));
    if (parsed_recordbatch != nullptr) {
        print_batch_excerpt(parsed_recordbatch);
    }

    return arrow::Status::OK();
}

int main(int argc, char **argv) {
    arrow::Result<BatchStreamRdrPtr> reader_result = (
        R_GetReaderForKey(std::string("expression:001518dd.0"))
    );

    arrow::Status print_status = R_PrintBatchExcerpt(reader_result);

    if (not print_status.ok()) {
        std::cerr << "Error: '" << print_status.ToString() << "'"
                  << std::endl
        ;

        return 1;
    }

    return 0;
}
*/
